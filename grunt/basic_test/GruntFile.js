module.exports = function(grunt) {

  grunt.initConfig({
    mkdir: {
      all: {
        options: {
          create: ['build']
        },
      },
    },

    clean: {
      build: {
        src: ['build']
      },

      regular: {
        src: ['build2']
      }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-mkdir');
  grunt.registerTask('build', ['clean:build', 'mkdir']);
  grunt.registerTask('regular', ['clean:regular', 'mkdir']);

};
