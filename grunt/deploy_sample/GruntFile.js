module.exports = function(grunt) {

  grunt.initConfig({
    clean: {
      all: {
        src: ['build']
      },
    },

    copy: {
      files: {
        src: ['src/**/*.js'],
        dest: 'build/',
        expand: true
      },
    },

    scp: {
      options: {
        host: '127.0.0.1',
        username: 'jmimora',
        privateKey: require('fs').readFileSync('/Users/jmimora/.ssh/id_rsa')
      },
      all: {
        files: [{
          cwd: 'build',
          filter: 'isFile',
          src: '**/*',
          dest: '/Users/jmimora/test2',
        }]
      },
    },
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-mkdir');
  grunt.loadNpmTasks('grunt-copy');
  grunt.loadNpmTasks('grunt-scp');

  grunt.registerTask('default', ['clean', 'copy', 'scp']);
};
